#!/usr/bin/env python3
# coding: utf-8

from pathlib import Path
import json

from cached_property import cached_property

from workman import AbstractWorkspace, AbstractExercise

# This code base, to modify according your needs, assumes
# that the upstream workspace is structured as follow
#
# root/exercises
#     ├── exo_a
#     │   ├── statement.rst
#     │   ├── example.py
#     │   ├── unittests.py
#     │   ├── code_base.py
#     │   └── metadata.json
#     └── exo_b
#         ├── statement.rst
#         ├── example.py
#         ├── unittests.py
#         ├── code_base.py
#         └── metadata.json


class Workspace(AbstractWorkspace):
    def _exercises_paths(self, repo_path):
        """ return a list (or a generator) over the exercises path
        """
        repo_path = Path(repo_path)
        return repo_path.glob('exercises/*')


class Exercise(AbstractExercise):
    @cached_property
    def statement(self) -> str:
        """ return a string describing, in restructuredText, the statement of
        the exercise
        """
        statement_filepath = self.exercise_data_path / 'statement.rst'
        with statement_filepath.open() as fobj:
            return fobj.read()

    @cached_property
    def example(self) -> str:
        # /path_to_exo/example.py

        example_file_path = self.exercise_data_path / 'example.py'
        with example_file_path.open() as fobj:
            return fobj.read()

    @cached_property
    def unittests(self) -> str:
        # /path_to_exo/exo_name_test.py

        unittests_file_path = self.exercise_data_path / 'unittests.py'
        with unittests_file_path.open() as fobj:
            return fobj.read()

    @cached_property
    def metadata(self) -> dict:
        # /path_to_exo/metadata.json

        upstream_metadata_path = self.exercise_data_path / 'metadata.json'
        with upstream_metadata_path.open() as fobj:
            upstream_metadata = json.load(fobj)

        metadata = self._default_metadata.copy()
        # we take only the metadata we are interested in
        metadata['slug'] = upstream_metadata['name']
        metadata['difficulty'] = int(upstream_metadata['difficulty'])
        metadata['topics'] = list(
            set(metadata['topics']) | set(upstream_metadata['topics'] or [])
        )

        return metadata

    @cached_property
    def code_base(self) -> str:
        code_base_path = self.exercise_data_path / 'code_base.py'
        with code_base_path.open() as fobj:
            return fobj.read()

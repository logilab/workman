# WORKSPACE_NAME Workspace

This workspace is based on the exercises provided by the [WORKSPACE_NAME
plateform](WORKSPACE_URL) under the WORKSPACE_LICENSE license.

This repository adapts the exercises from the mentionned plateform so they can
be used with the
[Jupyterlab-training](https://gitlab.com/logilab/jupyterlab-training) extension.

## Prerequisites

| Package               | Version |
| --------------------- | ------- |
| GitPython             | 2.1.11  |
| PyYAML                | 3.13    |
| Sphinx                | 1.8.3   |
| sphinxcontrib-jupyter | 0.3.0   |
| workman               |         |


## Updating the repo from upstream

```bash
workman --update
```

See `workman` documentation for more help (or run `workman -h`)

This command will clone the upstream repository, update the exercises and the
metadata in the *exercises/* directory if there is any change from upstream.

## Create the notebooks

```bash
make workspace
```

This command converts all the exercices (.md) to jupyternotebooks, and copies
all static data to the \_build directory (metadata.yml, .py, etc)

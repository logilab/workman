#!/usr/bin/env python3
# coding: utf-8

from uuid import uuid4
import re
import sys
from enum import Enum
import difflib
import subprocess
from pathlib import Path
from tempfile import mkstemp
from io import StringIO

import yaml


class EmptyExerciseError(ValueError):
    pass


class Action(Enum):
    """ Describe what to do on a conflict when updating
    """

    SKIP = 'skip'
    OVERWRITE = 'overwrite'
    MERGE = 'merge'


def handle_action(action, local_value, upstream_value, label):
    if action == Action.SKIP:
        return
    elif action == Action.OVERWRITE:
        # nothing to do, just write the upstream value
        return upstream_value
    elif action == Action.MERGE:
        # let's merge
        if local_value == upstream_value:
            return local_value

        return merge(local_value, upstream_value, label)


def merge(local, upstream, label):
    tmp_path = Path(mkstemp(suffix='.diff')[1])
    header = '\n'.join(
        [
            "# {}".format(label),
            "# edit this file as you want the final file to be",
            "# make sure that there is no line '+' or '-' left before leaving",
            "# leave two spaces at the beginning of each line",
            "# ",
            "# - : your data",
            "# + : upstream data",
            "  \n",
        ]
    )
    with tmp_path.open('w') as fobj:
        fobj.write(header)
        difflines = difflib.ndiff(
            local.strip().split('\n'), upstream.strip().split('\n')
        )
        fobj.write(
            '\n'.join(line for line in difflines if not line.startswith('?'))
        )

    is_valid = False
    while not is_valid:
        exit_code = subprocess.call('vim {}'.format(tmp_path).split())

        if exit_code > 0:
            sys.exit(exit_code)

        result = []
        with tmp_path.open() as fobj:
            for line_idx, line in enumerate(fobj):
                # line starts with a comment, or has been merged
                if line[0] not in ('#', ' '):  # file is not valid
                    break
                if line_idx < header.count('\n'):  # skip the header
                    continue
                result.append(line[2:])  # remove the two spaces
            else:
                is_valid = True
    tmp_path.unlink()
    return ''.join(result)


class AbstractExercise:
    """
    An exercise contains :
      - a statement (str)
      - a code_base (str)
      - a example (str)
      - unittests (str)
      - metadata (dict)
    """

    def __init__(self, workspace_name, exercise_data_path):
        """ Create an exercise from the data read in the `exercise_data_path`
        folder
        """
        self.exercise_data_path = exercise_data_path
        self.workspace_name = workspace_name
        self._default_metadata = {
            'slug': str(uuid4()),
            'difficulty': 1,
            'topics': [workspace_name],
            'unlocked_by': None,
        }

    @property
    def name(self):
        return self.metadata['slug'].replace('-', '_')

    def save(self, workspace_root_path, **save_options):
        output_path = workspace_root_path / self.metadata['slug']
        output_path.mkdir(exist_ok=True)

        if not self.statement:
            output_path.rmdir()
            raise EmptyExerciseError()

        actions = {k: Action.MERGE for k in 'SCEUM'}
        for key in save_options['skip']:
            actions[key] = Action.SKIP
        for key in save_options['overwrite']:
            actions[key] = Action.OVERWRITE

        self._save_statement(output_path, action=actions['S'])
        self._save_code_base(output_path, action=actions['C'])
        self._save_example(output_path, action=actions['E'])
        self._save_unittest(output_path, action=actions['U'])
        self._save_metadata(output_path, action=actions['M'])

    def _save_statement(self, output_path, action=Action.MERGE):
        statement_path = output_path / 'README.en.rst'
        upstream_statement = self.statement

        filecontent = upstream_statement
        filecontent += '''
.. literalinclude:: /{output_path}/code_base.py
                    :language: python
                    :class: exercise
'''.format(
            output_path=str(output_path)
        )

        if self.unittests:
            filecontent += '''

.. literalinclude:: /{output_path}/exercise_test.py
                    :language: python
                    :class: test

'''.format(
                output_path=str(output_path)
            )

        if self.example:
            filecontent += '''

**Solution**

.. literalinclude:: /{output_path}/example.py
                    :language: python
                    :class: solution
'''.format(
                output_path=str(output_path)
            )

        if self.footer:
            filecontent += '\n' + self.footer

        if statement_path.exists():
            with statement_path.open() as fobj:
                local_statement = fobj.read()

            filecontent = handle_action(
                action,
                local_statement,
                filecontent,
                'statement of {}'.format(self.name),
            )
            if not filecontent:
                return

        with statement_path.open('w') as fobj:
            fobj.write(filecontent)

    def _save_unittest(self, output_path, action=Action.MERGE):
        unittest_path = output_path / 'exercise_test.py'

        # look for the unittest class name
        prog = re.compile(r'class ([a-zA-Z_]+)\(unittest.TestCase\):')
        unittests_lines = self.unittests.split('\n')
        for line in unittests_lines:
            m = prog.search(line)
            if m:
                unittest_class_name = m.group(1)
                break
        else:  # no name found
            raise ValueError(
                'No unittest class found for exercises {}'.format(self.name)
            )

        filecontent = "# formation_unittest\n"
        module_imported = False
        for line in unittests_lines:
            # as we are using jupyter notebooks, no imports are need
            # from the unit tests
            if line.startswith('from {} import'.format(self.name)):
                continue
            elif line.startswith('import {}'.format(self.name)):
                module_imported = True
                continue
            if module_imported:
                # replace my_exo.my_function() by my_function
                line = line.replace('{}.'.format(self.name), '')
            filecontent += line + '\n'
        filecontent += (
            '# formation widget\n'
            'from jupyterlab_training import FormUnitTests\n'
            'FormUnitTests({});\n'.format(unittest_class_name)
        )

        if unittest_path.exists():
            with unittest_path.open() as fobj:
                local_content = fobj.read()

            filecontent = handle_action(
                action,
                local_content,
                filecontent,
                'unittest of {}'.format(self.name),
            )
            if not filecontent:
                return

        with unittest_path.open('w') as fobj:
            fobj.write(filecontent)

    def _save_example(self, output_path, action=Action.MERGE):
        example_path = output_path / 'example.py'
        filecontent = "# formation_solution\n" + self.example

        if example_path.exists():
            with example_path.open() as fobj:
                local_content = fobj.read()

            filecontent = handle_action(
                action,
                local_content,
                filecontent,
                'example of {}'.format(self.name),
            )
            if not filecontent:
                return

        with (output_path / 'example.py').open('w') as fobj:
            fobj.write(filecontent)

    def _save_metadata(self, output_path, action=Action.MERGE):
        metadata_path = output_path / 'metadata.yml'
        metadata = self.metadata

        if metadata_path.exists():
            upstream_metadata = metadata
            with metadata_path.open() as fobj:
                local_metadata = yaml.load(fobj)

            local_metadata['topics'] = sorted(local_metadata['topics'])
            upstream_metadata['topics'] = sorted(metadata['topics'])

            metadata_str = handle_action(
                action,
                yaml.dump(local_metadata),
                yaml.dump(upstream_metadata),
                'metadata of {}'.format(self.name),
            )

            if not metadata_str:
                return

            metadata = yaml.load(StringIO(metadata_str))

        # finally, save the file
        with (output_path / 'metadata.yml').open('w') as fobj:
            yaml.dump(metadata, fobj)

    def _save_code_base(self, output_path, action=Action.MERGE):
        code_base_path = output_path / 'code_base.py'
        filecontent = self.code_base

        if code_base_path.exists():
            with code_base_path.open() as fobj:
                local_content = fobj.read()

            filecontent = handle_action(
                action,
                local_content,
                filecontent,
                'code base of {}'.format(self.name),
            )
            if not filecontent:
                return

        with (output_path / 'code_base.py').open('w') as fobj:
            fobj.write(filecontent)

    def __str__(self):
        return self.metadata['slug']

    @property
    def code_base(self) -> str:
        return "# write you code here\n"

    @property
    def statement(self) -> str:
        """ statement written in restructuredText """
        return ''

    @property
    def example(self) -> str:
        """ statement written in python """
        return ''

    @property
    def unittests(self) -> str:
        """ statement written in python """
        return ''

    @property
    def metadata(self) -> dict:
        return self._default_metadata

    @property
    def footer(self) -> dict:
        return 'This exercise is provided by {}'.format(self.workspace_name)

# WORKMAN, a remote WORKspace MANanger

*WORKMAN* is a tool used to create workspaces that work within the [Jupyter
Training environment](https://gitlab.com/logilab/jupyterlab-training-workspace).

The goal of *WORKMAN* is to let you create workspace from a remote repository,
that you can keep up-to-date from upstream while having local modification.

*WORKMAN* provides python classes that can be used to describe a serie of
exercises from a remote repository, and them save those exercises into a local
folder. Then you can modify the content of the exercises (statement, solution,
unittest, metadata, etc). If you want to update your local copie of exercises,
*WORKMAN* will help you. You can choose, individually for statement, solution,
etc to:

1. overwrite your local version
2. keep your local version
3. make a merge between your local version and the upstream one.

## Prerequisites

    GitPython 2.1.11
    PyYAML    3.13
    cached_property

## Installation

Create a virtualenv and install dependencies

```bash
>> python3 -m venv my_env
>> . ./my_env/bin/activate
(my_env)>> cd workman
(my_env)>> pip install -r requirements.txt
(my_env)>> pip install .
```

## Usage example

```bash
(my_env)>> mkdir example_workspace
(my_env)>> cd example_workspace
(my_env)>> workman --init
upstream url: http://example.fr/a/remote_repository_of_exercises.git
workspace name: example
workspace license: MIT
(my_env)>>
```

This will create the base files. One of them is `.workman.json` which
contains the data you provided below. At this stage, you can add this
directory to your versionning tool.

Then, you have to modify the `workspace.py` file according to the structure
of the remote repository, describe where are located the exercises, how to
read the statement, the solution (if any), the unittests (if any) and so
on. Once this is done, you can get the first version of the exercises doing :

```bash
(my_env)>>> workman --update
```

This will clone the repository, and the save the exercises into an `exercises/`
directory. Then, you can make local modification, such a modifying the
statement, the metadata, etc. (Remember to save your changes with a versionning
tool)

If you want to update your local version from the upstream repository, rerun the
`workman --update` command. By default, in case of a change between your local
version and the upstream one, a merge is suggested. You have to examine the
difference, and keep what you want. But the are options to skip or to overwrite.

Each exercises a made of:

* a statement file (S)
* a metadata file (M)
* a code base file (C)
* an example file (solution) (E)
* a unittest file (U)

If you want to overwrite the statement (S), and the code base (C) of your local
version, you can do the following:

```bash
(my_env)>> workman --update --overwrite S C
```

The statement and the code base will be overwritten, but a merge (in case of
changes) will be done for metadata (M), solution (E) and unittest (T)

In the same manner, you can skip the upstream changes, and keep your version:
```bash
(my_env)>> workman --update --skip M --overwrite S C E T
```
In that case, everything will be overwritten, but your local version of the
metatada will be kept.

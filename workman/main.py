#!/usr/bin/env python3
# coding: utf-8

import argparse
import pathlib
import sys

from .manager import init_workspace, update_workspace


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i',
        '--init',
        dest='init',
        action='store_true',
        help='initialize the workspace (to be used to create the repo)',
    )
    parser.add_argument(
        '-u',
        '--update',
        dest='update',
        action='store_true',
        help='update an existing workspace',
    )

    parser.add_argument(
        '-o',
        '--overwrite',
        dest='overwrite',
        action='store',
        help='Overwrite from uptream',
        nargs='+',
        default=[],
    )

    parser.add_argument(
        '-s',
        '--skip',
        dest='skip',
        action='store',
        nargs='+',
        help='Keep local version',
        default=[],
    )

    parser.add_argument(
        '-m',
        '--merge',
        dest='merge',
        action='store',
        nargs='+',
        help='Look up for merge',
        default=[],
    )

    args = parser.parse_args()
    return args


def is_in_a_workspace_repo():
    return pathlib.Path('.workman.json').exists()


def main():
    args = parse_args()

    if args.init:
        upstream_url = input('upstream url: ')
        workspace_name = input('workspace name: ')
        workspace_license = input('workspace license: ')

        assert upstream_url, "cannot be empty"
        assert workspace_name, "cannot be empty"
        assert workspace_license, "cannot be empty"

        return init_workspace(
            workspace_name, upstream_url, workspace_license, pathlib.Path('.')
        )

    if not is_in_a_workspace_repo():
        print(
            "Please use this command inside a workspace repository",
            file=sys.stderr,
        )
        sys.exit(1)

    if args.update:
        options = {
            'overwrite': args.overwrite,
            'merge': args.merge,
            'skip': args.skip,
        }
        update_workspace(pathlib.Path('.'), **options)

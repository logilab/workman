#!/usr/bin/env python3
# coding: utf-8

import tempfile
from git import Repo

from .exercises import EmptyExerciseError


class AbstractWorkspace:
    """ An abstract class used to create a workspace from an exercises
    repository
    """

    def __init__(self, workspace_name, git_repo_url, Exercise_cls):
        self.workspace_name = workspace_name
        self.git_repo_url = git_repo_url
        self.Exercise_cls = Exercise_cls

    def _exercises_paths(self, repo_path):
        """ Iterate over the paths targetting an exercise
            The returned path will be given to `Exercise_cls` to instanciate an
            exercise.

            The implementation depends on the workspace
        """
        raise NotImplementedError()

    @property
    def exercises(self):
        with tempfile.TemporaryDirectory() as tmp_repo_path:
            Repo.clone_from(self.git_repo_url, tmp_repo_path)

            for exercise_path in self._exercises_paths(tmp_repo_path):
                exercise = self.Exercise_cls(
                    self.workspace_name, exercise_path
                )
                yield exercise

    def save(self, outputdirectory_path, **save_options):
        outputdirectory_path.mkdir(exist_ok=True)
        for exercise in self.exercises:
            try:
                exercise.save(outputdirectory_path, **save_options)
            except EmptyExerciseError:
                print(
                    'The exercise {} is empty, skip it'.format(exercise.name)
                )

#!/usr/bin/env python3
# coding: utf-8

import json
import pathlib
import sys


def _is_empty(directory_path):
    try:
        next(directory_path.glob('*'))
    except StopIteration:
        return True
    return False


def init_workspace(name, upstream_url, license, workspace_path):
    if not _is_empty(workspace_path):
        raise OSError("the directory must be empty")

    root = pathlib.Path(__file__).parent
    for a_file in (root / 'empty_workspace').glob('*'):
        with a_file.open() as fobj:
            content = fobj.read()
            content = content.replace('WORKSPACE_NAME', name)
            content = content.replace('WORKSPACE_URL', upstream_url)
            content = content.replace('WORKSPACE_LICENSE', license)

        with (workspace_path / a_file.name).open('w') as fobj:
            fobj.write(content)

    with (workspace_path / '.workman.json').open('w') as fobj:
        data = {
            'name': name,
            'upstream_url': upstream_url,
            'license': license,
        }
        json.dump(data, fobj)


def update_workspace(workspace_path, **update_options):
    sys.path.insert(0, '.')

    from workspace import Workspace, Exercise

    with (workspace_path / '.workman.json').open() as fobj:
        data = json.load(fobj)

    workspace = Workspace(data['name'], data['upstream_url'], Exercise)
    workspace.save(pathlib.Path('./exercises'), **update_options)

    sys.path = sys.path[1:]  # remove '.' from sys.path

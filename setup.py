#!/usr/bin/env python
# coding: utf-8

from setuptools import find_packages, setup

distname = 'workman'
version = '0.1'
license = 'GPLv3'
description = (
    'tools to manage a local copy of a remote workspace '
    '(WORKspace MANager)'
)
author = 'Logilab'
author_email = 'contact@logilab.fr'
requires = {}

setup(
    name=distname,
    version=version,
    license=license,
    description=description,
    author=author,
    author_email=author_email,
    packages=find_packages(exclude=['test']),
    include_package_data=True,
    zip_safe=False,
    entry_points={'console_scripts': ['workman = workman.main:main']},
)
